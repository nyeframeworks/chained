﻿using Definitions.Camera;
using Systems.Shared.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using Core.Shared.Components;

namespace Core.Shared.Camera
{
    public class Camera : Core.Shared.System, ICamera
    {
        private readonly GraphicsDevice _GraphicsDevice;

        public TransformComponent TransformComponent { get; private set; }

        public Vector2 GetWorldPosition(Vector2 mousePosition)
        {
            Matrix inverseViewMatrix = TransformComponent.InvertAbsolute;
            return Vector2.Transform(mousePosition, inverseViewMatrix);
        }

        public Vector2 Size => new Vector2(_GraphicsDevice.Viewport.Width, _GraphicsDevice.Viewport.Height);

        public Rectangle Bounds => new Rectangle(Position.ToPoint() - (Size / 2).ToPoint(), Size.ToPoint());

        // Get set position
        public Vector2 Position
        {
            get => TransformComponent.Position;
            set => TransformComponent.Position = value;
        }

        public Camera(GraphicsDevice graphicsDevice)
        {
            _GraphicsDevice = graphicsDevice;
        }

        public override void SetWorld(IWorld world)
        {
            base.SetWorld(world);
            TransformComponent = new TransformComponent("Camera", new List<Tuple<String, Object>>())
            {
                Position = new Vector2(0, 0),
                Scale = Vector2.One,
                Rotation = 0
            };
            TransformComponent.SetWorld(world);
        }

        public void Dispose()
        {

        }
    }
}
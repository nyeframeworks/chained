﻿using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Graphics;
using Core.Shared.Input;
using Core.Shared.Logic;
using Core.Shared.Update;
using Core.Shared.World.Data;
using Definitions.Camera;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using Systems.Shared.World;
using Random = Core.Shared.Logic.Random;

namespace Core.Shared
{
    public abstract class ChainedGame : Game
    {
        protected GraphicsDeviceManager DeviceManager { get; private set; }

        protected IWorld World { get; private set; }

        protected ChainedGame(Point screenSize)
        {
            DeviceManager = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = screenSize.X,
                PreferredBackBufferHeight = screenSize.Y,
                SupportedOrientations = DisplayOrientation.Portrait | DisplayOrientation.PortraitDown
            };
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            Data.Init(Content);
            World = CreateWorld(CreateCamera());
            OnWorldCreated(World);
            base.Initialize();
        }

        protected virtual ICamera CreateCamera()
        {
            return new Camera.Camera(DeviceManager.GraphicsDevice);
        }

        protected virtual IWorld CreateWorld(ICamera camera)
        {
            var world = new World.World();
            world.AddSystem(new GraphicsSystem(DeviceManager));
            world.AddSystem(new ContentSystem(Content));
            world.AddSystem(camera);
            world.AddSystem(new InputSystem(camera));
            world.AddSystem(new DrawSystem(camera));
            world.AddSystem(new UpdateSystem());
            world.AddSystem(new Random(Environment.TickCount));
            world.AddSystem(new Distribution());

            world.SetCreator(new XMLEntityCreator());
            return world;
        }

        protected virtual void OnWorldCreated(IWorld world)
        {

        }

        protected override void LoadContent()
        {
            base.LoadContent();

            var assemblies = new List<Assembly>();
            assemblies.Add(Assembly.GetAssembly(GetGameType()));

            EntityStore.RegisterAll(assemblies);
            ComponentStore.RegisterAll(assemblies);

            OnLoadContent();
        }

        protected abstract Type GetGameType();

        protected abstract void OnLoadContent();

        protected override void UnloadContent()
        {
            base.UnloadContent();
            OnUnloadContent();
        }

        protected abstract void OnUnloadContent();

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            World.Update(gameTime.ElapsedGameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            World.DrawAll();
            base.Draw(gameTime);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    [Component("Active")]
    public class ActiveComponent : SimpleComponent<Boolean>
    {
        public Boolean ParentActive { get; set; }

        public ActiveComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            ParentActive = true;
        }
    }
}

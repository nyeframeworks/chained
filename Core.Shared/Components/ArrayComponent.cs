﻿using Core.Shared.Packages;
using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    public abstract class ArrayComponent<T> : Component
    {
        public T[] Value { get; }

        protected ArrayComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            Value = values.GetArray<T>(name);
        }
    }
}

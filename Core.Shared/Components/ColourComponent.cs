﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    [Component("Colour")]
    public class ColourComponent : SimpleComponent<Color>
    {
        public ColourComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}

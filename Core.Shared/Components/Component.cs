﻿using System;
using System.Collections.Generic;
using Systems.Shared.World;

namespace Core.Shared.Components
{
    public abstract class Component : IComponent
    {
        protected IEntity Owner { get; private set; }

        protected IWorld World { get; private set; }

        private String _Name;

        private List<Tuple<String, Object>> _Values;

        protected Component(String name, List<Tuple<String, Object>> values)
        {
            _Values = values;
        }

        public void SetOwner(IEntity entity)
        {
            Owner = entity;
        }

        public void SetWorld(IWorld world)
        {
            World = world;
        }

        public virtual void Start()
        {

        }

        public virtual void Dispose()
        {

        }
    }
}

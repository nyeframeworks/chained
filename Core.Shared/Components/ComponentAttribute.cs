﻿using System;

namespace Core.Shared.Components
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ComponentAttribute : Attribute
    {
        public ComponentAttribute(String tag)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Shared.Components
{
    public static class ComponentStore
    {
        private static readonly Dictionary<String, Type> RegisteredComponents = new Dictionary<string, Type>();

        public static void RegisterAll(List<Assembly> assemblies)
        {
            foreach (var typeInfo in assemblies.SelectMany(s => s.DefinedTypes).Where(s => s.CustomAttributes.Any(a => a.AttributeType == typeof(ComponentAttribute))))
            {
                var ca = typeInfo.CustomAttributes.First(s => s.AttributeType == typeof(ComponentAttribute));
                Add((String)ca.ConstructorArguments.First().Value, typeInfo);
            }
        }

        public static void Add(String tag, Type type)
        {
            RegisteredComponents.Add(tag, type);
        }

        public static Boolean Available(String dataKey)
        {
            return RegisteredComponents.ContainsKey(dataKey);
        }

        public static Object Creator(String tag, List<Tuple<String, Object>> data)
        {
            return Activator.CreateInstance(RegisteredComponents[tag], tag, data);
        }
    }
}

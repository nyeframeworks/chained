﻿using Core.Shared.Packages;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using Systems.Shared.Graphics;

namespace Core.Shared.Components
{
    [Component("Draw")]
    [RequiresComponents(new[] { typeof(VisibleComponent), typeof(TransformComponent) })]
    public class DrawComponent : Component
    {
        public enum DrawType
        {
            Texture2D,
            SpriteText
        };

        public DrawType Type { get; private set; }

        public Texture2D Texture { get; set; }

        private readonly String _Source;

        public Single Z { get; private set; }

        public DrawComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            Type = values.GetValue<DrawType>($"{name}-Type");
            _Source = values.GetValue<String>($"{name}-Source");

            Z = values.GetValue<Single>($"{name}-Z");
        }

        public override void Start()
        {
            base.Start();

            switch (Type)
            {
                case DrawType.Texture2D:
                    if (Texture == null && !String.IsNullOrEmpty(_Source)) Texture = World.GetSystem<IContentSystem>().Content.Load<Texture2D>(_Source);
                    break;
                case DrawType.SpriteText:
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    [Component("Name")]
    public class NameComponent : SimpleComponent<String>
    {
        public NameComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}

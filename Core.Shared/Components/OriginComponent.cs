﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Core.Shared.Components
{
    [Component("Origin")]
    public class OriginComponent : SimpleComponent<Vector2>
    {
        public OriginComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}

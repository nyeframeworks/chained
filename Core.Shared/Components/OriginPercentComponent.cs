﻿using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    [Component("OriginPercent")]
    public class OriginPercentComponent : OriginComponent
    {
        public OriginPercentComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {

        }
    }
}

﻿using System;

namespace Core.Shared.Components
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class RequiresComponentsAttribute : Attribute
    {
        public RequiresComponentsAttribute(params Type[] componentTypes)
        {

        }
    }
}

﻿using Core.Shared.Packages;
using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    public class SimpleComponent<T> : Component
    {
        private T _Value;

        public T Value
        {
            get => _Value;
            set
            {
                _Value = value;
                OnValueChanged(_Value);
            }
        }

        protected virtual void OnValueChanged(T newValue)
        {

        }

        public SimpleComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            Value = values.GetValue<T>(name);
        }
    }
}

﻿using Core.Shared.Packages;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

//https://github.com/aloisdeniel/Transform/blob/master/src/Transform/Transform2D.cs
namespace Core.Shared.Components
{

    [Component("Transform")]
    public class TransformComponent : Component
    {
        #region Constructors

        public TransformComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            _LocalPosition = values.GetValue<Vector2>($"{name}-Local");
            _AbsolutePosition = values.GetValue<Vector2>($"{name}-Abs");

            _LocalScale = values.GetValue<Vector2>($"{name}-Scale");

            _LocalRotation = values.GetValue<Single>($"{name}-Rotation");
        }

        #endregion

        #region Fields

        private TransformComponent _Parent;

        private readonly List<TransformComponent> _Children = new List<TransformComponent>();

        private Matrix _Absolute, _InvertAbsolute, _Local;

        private float _LocalRotation, _AbsoluteRotation;

        private Vector2 _LocalScale, _AbsoluteScale, _LocalPosition, _AbsolutePosition;

        private bool _NeedsAbsoluteUpdate = true, _NeedsLocalUpdate = true;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the parent transform.
        /// </summary>
        /// <value>The parent.</value>
        public TransformComponent Parent
        {
            get => _Parent;
            set
            {
                if (_Parent == value) return;
                _Parent?._Children.Remove(this);

                _Parent = value;

                _Parent?._Children.Add(this);

                SetNeedsAbsoluteUpdate();
            }
        }

        /// <summary>
        /// Gets all the children transform.
        /// </summary>
        /// <value>The children.</value>
        public IEnumerable<TransformComponent> Children => _Children;

        /// <summary>
        /// Gets the absolute rotation.
        /// </summary>
        /// <value>The absolute rotation.</value>
        public float AbsoluteRotation => UpdateAbsoluteAndGet(ref _AbsoluteRotation);

        /// <summary>
        /// Gets the absolute scale.
        /// </summary>
        /// <value>The absolute scale.</value>
        public Vector2 AbsoluteScale => UpdateAbsoluteAndGet(ref _AbsoluteScale);

        /// <summary>
        /// Gets the absolute position.
        /// </summary>
        /// <value>The absolute position.</value>
        public Vector2 AbsolutePosition => UpdateAbsoluteAndGet(ref _AbsolutePosition);

        /// <summary>
        /// Gets or sets the rotation (relative to the parent, absolute if no parent).
        /// </summary>
        /// <value>The rotation.</value>
        public float Rotation
        {
            get => _LocalRotation;
            set
            {
                if (!(Math.Abs(_LocalRotation - value) > Single.Epsilon)) return;
                _LocalRotation = value;
                SetNeedsLocalUpdate();
            }
        }

        /// <summary>
        /// Gets or sets the position (relative to the parent, absolute if no parent).
        /// </summary>
        /// <value>The position.</value>
        public Vector2 Position
        {
            get => _LocalPosition;
            set
            {
                if (_LocalPosition != value)
                {
                    _LocalPosition = value;
                    SetNeedsLocalUpdate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the scale (relative to the parent, absolute if no parent).
        /// </summary>
        /// <value>The scale.</value>
        public Vector2 Scale
        {
            get => _LocalScale;
            set
            {
                if (_LocalScale != value)
                {
                    _LocalScale = value;
                    SetNeedsLocalUpdate();
                }
            }
        }

        /// <summary>
        /// Gets the matrix representing the local transform.
        /// </summary>
        /// <value>The relative matrix.</value>
        public Matrix Local => UpdateLocalAndGet(ref _Absolute);

        /// <summary>
        /// Gets the matrix representing the absolute transform.
        /// </summary>
        /// <value>The absolute matrix.</value>
        public Matrix Absolute => UpdateAbsoluteAndGet(ref _Absolute);

        /// <summary>
        /// Gets the matrix representing the invert of the absolute transform.
        /// </summary>
        /// <value>The absolute matrix.</value>
        public Matrix InvertAbsolute => UpdateAbsoluteAndGet(ref _InvertAbsolute);

        #endregion

        #region Methods

        public void ToLocalPosition(ref Vector2 absolute, out Vector2 local)
        {
            Vector2.Transform(ref absolute, ref _InvertAbsolute, out local);
        }

        public void ToAbsolutePosition(ref Vector2 local, out Vector2 absolute)
        {
            Vector2.Transform(ref local, ref this._Absolute, out absolute);
        }

        public Vector2 ToLocalPosition(Vector2 absolute)
        {
            ToLocalPosition(ref absolute, out var result);
            return result;
        }

        public Vector2 ToAbsolutePosition(Vector2 local)
        {
            ToAbsolutePosition(ref local, out var result);
            return result;
        }

        private void SetNeedsLocalUpdate()
        {
            _NeedsLocalUpdate = true;
            SetNeedsAbsoluteUpdate();
        }

        private void SetNeedsAbsoluteUpdate()
        {
            _NeedsAbsoluteUpdate = true;

            foreach (var child in _Children)
            {
                child.SetNeedsAbsoluteUpdate();
            }
        }

        private void UpdateLocal()
        {
            var result = Matrix.CreateScale(Scale.X, Scale.Y, 1);
            result *= Matrix.CreateRotationZ(Rotation);
            result *= Matrix.CreateTranslation(Position.X, Position.Y, 0);
            _Local = result;

            _NeedsLocalUpdate = false;
        }

        private void UpdateAbsolute()
        {
            if (Parent == null)
            {
                _Absolute = _Local;
                _AbsoluteScale = _LocalScale;
                _AbsoluteRotation = _LocalRotation;
                _AbsolutePosition = _LocalPosition;
            }
            else
            {
                var parentAbsolute = Parent.Absolute;
                Matrix.Multiply(ref _Local, ref parentAbsolute, out _Absolute);
                _AbsoluteScale = Parent.AbsoluteScale * Scale;
                _AbsoluteRotation = Parent.AbsoluteRotation + Rotation;
                _AbsolutePosition = Vector2.Zero;
                ToAbsolutePosition(ref _AbsolutePosition, out _AbsolutePosition);
            }

            Matrix.Invert(ref _Absolute, out _InvertAbsolute);

            _NeedsAbsoluteUpdate = false;
        }

        private T UpdateLocalAndGet<T>(ref T field)
        {
            if (_NeedsLocalUpdate)
            {
                UpdateLocal();
            }

            return field;
        }

        private T UpdateAbsoluteAndGet<T>(ref T field)
        {
            if (_NeedsLocalUpdate)
            {
                UpdateLocal();
            }

            if (_NeedsAbsoluteUpdate)
            {
                UpdateAbsolute();
            }

            return field;
        }

        #endregion

    }
}
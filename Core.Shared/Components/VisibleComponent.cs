﻿using Core.Shared.Packages;
using System;
using System.Collections.Generic;

namespace Core.Shared.Components
{
    [Component("Visible")]
    public class VisibleComponent : Component
    {
        public Boolean ParentVisible { get; set; }

        public Boolean Visible
        {
            get => _Visible && ParentVisible;
            set => _Visible = value;
        }

        private Boolean _Visible;

        public VisibleComponent(string name, List<Tuple<string, object>> values) : base(name, values)
        {
            _Visible = values.GetValue<Boolean>("Visible");
            ParentVisible = true;
        }
    }
}

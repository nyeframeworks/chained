﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.IO;

namespace Core.Shared
{
    public static class Data
    {
        private static ContentManager _content;

        public static void Init(ContentManager content)
        {
            _content = content;
        }

        public static String ReadAll(String filePath)
        {
            filePath = filePath.TrimStart($"{_content.RootDirectory}/".ToCharArray());
            var path = Path.Combine(_content.RootDirectory, filePath);
            String text = "";
            using (var stream = TitleContainer.OpenStream(path))
            {
                using (var reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }
    }
}

﻿using Core.Shared.Components;
using Core.Shared.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Systems.Shared;
using Systems.Shared.World;

namespace Core.Shared.Entities
{
    /// <summary>An Entity consists of multiple <see cref="Component"/>'s</summary>
    [RequiresComponents(new Type[] { typeof(ActiveComponent), typeof(VisibleComponent), typeof(TransformComponent) })]
    public abstract class Entity : IEntity
    {
        private static Int64 _ID = 0;

        protected readonly List<IEntity> Entities = new List<IEntity>();

        protected readonly List<IComponent> Components = new List<IComponent>();

        protected readonly Package Package;

        protected IWorld World { get; private set; }

        public virtual Boolean Visible
        {
            get => GetComponent<VisibleComponent>().Visible;
            set
            {
                GetComponent<VisibleComponent>().Visible = value;
                foreach (var gameObject in Entities) gameObject.SetParentVisible(Visible);
            }
        }

        public virtual Boolean Active
        {
            get => GetComponent<ActiveComponent>().Value && GetComponent<ActiveComponent>().ParentActive;
            set
            {
                GetComponent<ActiveComponent>().Value = value;
                foreach (var gameObject in Entities) gameObject.SetParentActive(Active);
            }
        }

        public string Name { get; }

        public Int64 ID { get; private set; }

        protected Entity(Package package)
        {
            ID = _ID++;

            Package = package;

            Dictionary<String, List<Tuple<String, Object>>> componentData = new Dictionary<string, List<Tuple<string, Object>>>();

            List<Package> subentityPackages = new List<Package>();

            foreach (var data in Package.Data)
            {
                if (data.Item2 is Package p)
                {
                    subentityPackages.Add(p);
                }
                else
                {
                    var componentName = data.Item1.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).First();
                    if (componentName == package.Type) continue;

                    if (!componentData.ContainsKey(componentName)) componentData.Add(componentName, new List<Tuple<string, Object>>());
                    componentData[componentName].Add(data);
                }
            }

            foreach (var data in componentData)
            {
                //If there is no registered component of the data given, then it can be handled via the Package data
                if (!ComponentStore.Available(data.Key)) continue;

                var c = (Component)ComponentStore.Creator(data.Key, data.Value);
                c.SetOwner(this);
                Components.Add(c);
            }

            ValidateEntity(Components.ToArray());

            ValidateAllComponents(Components.ToArray());

            Name = GetComponent<NameComponent>()?.Value ?? $"{Guid.NewGuid()}{GetType()}";

            foreach (var p in subentityPackages)
            {
                var o = EntityStore.Creator(p.Type, p);
                Attach((Entity)o);
            }
        }

        private void ValidateEntity(IComponent[] components)
        {
            List<Type> attachedComponentTypes = components.Select(s => s.GetType()).ToList();

            List<CustomAttributeData> entityRequiredComponents = new List<CustomAttributeData>();
            Type entityType = GetType();
            while (entityType != typeof(Object))
            {
                entityRequiredComponents.AddRange(entityType.CustomAttributes.Where(s => s.AttributeType == typeof(RequiresComponentsAttribute)).ToList());
                entityType = entityType.BaseType;
            }

            List<Type> requiredComponentTypes = new List<Type>();

            //Collate all the types that are placed in the ComponentRequiredAttribute
            foreach (var rca in entityRequiredComponents.SelectMany(s => s.ConstructorArguments).Select(s => s.Value).ToArray())
            {
                foreach (var cct in (IEnumerable<CustomAttributeTypedArgument>)rca)
                {
                    requiredComponentTypes.Add((Type)cct.Value);
                }
            }

            //Which ones are missing from the attachedComponents that actually exists on this Entity?
            foreach (var type in requiredComponentTypes.Except(attachedComponentTypes).ToList())
            {
                Boolean ignore = false;
                foreach (var attached in attachedComponentTypes)
                {
                    if (!type.IsAssignableFrom(attached)) continue;
                    ignore = true;
                    break;
                }
                if (ignore) continue;

                //Throw an exception for each missing component that is required
                throw new Exception($"{type} is required for {GetType()}");
            }
        }

        private void ValidateAllComponents(IComponent[] components)
        {
            List<Type> attachedComponentTypes = components.Select(s => s.GetType()).ToList();

            //Loop through all components and find all the RequiredComponentAttributes
            foreach (var component in components)
            {
                List<CustomAttributeData> requiredComponentsAttributes = new List<CustomAttributeData>();
                Type compType = component.GetType();
                while (compType != typeof(Object))
                {
                    requiredComponentsAttributes.AddRange(compType.CustomAttributes.Where(s => s.AttributeType == typeof(RequiresComponentsAttribute)).ToList());
                    compType = compType.BaseType;
                }

                if (!requiredComponentsAttributes.Any()) continue;

                //Check all the attached components, and see if they are all there

                List<Type> requiredComponentTypes = new List<Type>();

                //Collate all the types that are placed in the ComponentRequiredAttribute
                foreach (var rca in requiredComponentsAttributes.SelectMany(s => s.ConstructorArguments).Select(s => s.Value).ToArray())
                {
                    foreach (var cct in (IEnumerable<CustomAttributeTypedArgument>)rca)
                    {
                        requiredComponentTypes.Add((Type)cct.Value);
                    }
                }

                //Which ones are missing from the attachedComponents that actually exists on this Entity?
                foreach (var type in requiredComponentTypes.Except(attachedComponentTypes).ToList())
                {
                    Boolean ignore = false;
                    foreach (var attached in attachedComponentTypes)
                    {
                        if (!type.IsAssignableFrom(attached)) continue;
                        ignore = true;
                        break;
                    }
                    if (ignore) continue;

                    //Throw an exception for each missing component that is required
                    throw new Exception($"{type} is required for {component} part of entity {GetType()}");
                }
            }

        }

        public void Attach<T>(T entity) where T : IEntity
        {
            if (Entities.Any(s => s.Name == entity.Name)) throw new Exception("");
            entity.SetParentActive(Active);
            entity.SetParentVisible(Visible);

            var child = entity.GetComponent<TransformComponent>();
            var my = GetComponent<TransformComponent>();
            child.Parent = my;

            Entities.Add(entity);
        }

        public void Detatch<T>(T entity) where T : IEntity
        {
            if (!Entities.Contains(entity)) return;
            Entities.Remove(entity);
            entity.SetParentActive(true);
            entity.SetParentVisible(true);

            entity.GetComponent<TransformComponent>().Parent = null;
        }

        public virtual void Start()
        {
            foreach (var component in Components)
            {
                component.Start();
            }

            foreach (var entity in Entities)
            {
                entity.Start();
                World.BroadcastMessage(new Message("EntityCreated", entity), typeof(ISystem));
            }
        }

        public void SetWorld(IWorld world)
        {
            World = world;

            foreach (var entity in Entities)
            {
                entity.SetParentActive(Active);
                entity.SetParentVisible(Visible);
                entity.SetWorld(World);
            }

            foreach (var component in Components.ToList())
            {
                component.SetWorld(world);
            }

            OnWorldSet();
        }

        protected virtual void OnWorldSet() { }

        public void SetParentVisible(Boolean parentVisible)
        {
            GetComponent<VisibleComponent>().ParentVisible = parentVisible;
            foreach (var gameObject in Entities) gameObject.SetParentVisible(Visible);
        }

        public void SetParentActive(Boolean active)
        {
            GetComponent<ActiveComponent>().ParentActive = active;
            foreach (var gameObject in Entities) gameObject.SetParentActive(Active);
        }

        public virtual void Update(TimeSpan deltaTime)
        {

        }

        public T GetComponent<T>(Func<T, Boolean> predicate = null) where T : IComponent
        {
            return Components.OfType<T>().FirstOrDefault(s => predicate == null || predicate.Invoke(s));
        }

        public IComponent[] GetComponents(Func<IComponent, Boolean> predicate = null)
        {
            return Components.Where(s => predicate == null || predicate.Invoke(s)).ToArray();
        }

        public T GetChild<T>(Func<T, Boolean> predicate = null) where T : IEntity
        {
            return Entities.OfType<T>().First(s => predicate == null || predicate.Invoke(s));
        }

        public IEntity[] GetChildren(Func<IEntity, Boolean> predicate = null)
        {
            return Entities.Where(s => predicate == null || predicate.Invoke(s)).ToArray();
        }

        public virtual void MessageReceived(Message message)
        {

        }

        public virtual void Dispose()
        {
            World.Destroy(this);
            foreach (var component in Components)
            {
                component.Dispose();
            }

            foreach (var entity in Entities)
            {
                entity.Dispose();
            }
        }

        public override string ToString()
        {
            return Name ?? base.ToString();
        }

        protected String EntityName()
        {
            var ea = GetType().CustomAttributes.First(s => s.AttributeType == typeof(EntityAttribute));
            return ea.ConstructorArguments.First().Value as String;
        }

        ~Entity()
        {

        }
    }
}

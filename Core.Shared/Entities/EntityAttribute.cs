﻿using System;

namespace Core.Shared.Entities
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityAttribute : Attribute
    {
        public EntityAttribute(String tag)
        {

        }
    }
}

﻿using Core.Shared.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Shared.Entities
{
    public static class EntityStore
    {
        private static readonly Dictionary<String, Type> RegisteredObjects = new Dictionary<string, Type>();

        public static void RegisterAll(List<Assembly> assemblies)
        {
            foreach (var typeInfo in assemblies.SelectMany(s => s.DefinedTypes).Where(s => s.CustomAttributes.Any(a => a.AttributeType == typeof(EntityAttribute))))
            {
                var ca = typeInfo.CustomAttributes.First(s => s.AttributeType == typeof(EntityAttribute));
                Add((String)ca.ConstructorArguments.First().Value, typeInfo);
            }
        }

        public static void Add(String tag, Type type)
        {
            RegisteredObjects.Add(tag, type);
        }

        public static Object Creator(String tag, Package package)
        {
            return Activator.CreateInstance(RegisteredObjects[tag], package);
        }
    }
}

using System;

namespace Core.Shared.Glide.Data
{
	/// <summary>
	/// Static class with useful easer functions that can be used by Tweens.
	/// </summary>
	public static class Ease
	{
		const Single Pi = 3.14159f;
		const Single Pi2 = Pi / 2;
		const Single B1 = 1 / 2.75f;
		const Single B2 = 2 / 2.75f;
		const Single B3 = 1.5f / 2.75f;
		const Single B4 = 2.5f / 2.75f;
		const Single B5 = 2.25f / 2.75f;
		const Single B6 = 2.625f / 2.75f;
		
		/// <summary>
		/// Ease a value to its target and then back. Use this to wrap another easing function.
		/// </summary>
		public static Func<Single, Single> ToAndFro(Func<Single, Single> easer)
		{
			return t => ToAndFro(easer(t));
		}
		
		/// <summary>
		/// Ease a value to its target and then back.
		/// </summary>
		public static Single ToAndFro(Single t)
		{
			return t < 0.5f ? t * 2 : 1 + ((t - 0.5f) / 0.5f) * -1;
		}
		
		/// <summary>
		/// Elastic in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ElasticIn(Single t)
		{
			return (Single) ( Math.Sin(13 * Pi2 * t) * Math.Pow(2, 10 * (t - 1)));
		}
		
		/// <summary>
		/// Elastic out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ElasticOut(Single t)
		{
			if (t == 1) return 1;
			return (Single) ( Math.Sin(-13 * Pi2 * (t + 1)) * Math.Pow(2, -10 * t) + 1);
		}
		
		/// <summary>
		/// Elastic in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ElasticInOut(Single t)
		{
			if(t < 0.5)
			{
				return (Single) ( 0.5 * Math.Sin(13 * Pi2 * (2 * t)) * Math.Pow(2, 10 * ((2 * t) - 1)));
			}
			
			return (Single) ( 0.5 * (Math.Sin(-13 * Pi2 * ((2 * t - 1) + 1)) * Math.Pow(2, -10 * (2 * t - 1)) + 2));
		}
		
		/// <summary>
		/// Quadratic in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuadIn(Single t)
		{
			return (Single) ( t * t);
		}
		
		/// <summary>
		/// Quadratic out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuadOut(Single t)
		{
			return (Single) ( -t * (t - 2));
		}
		
		/// <summary>
		/// Quadratic in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuadInOut(Single t)
		{
			return (Single) ( t <= .5 ? t * t * 2 : 1 - (--t) * t * 2);
		}
		
		/// <summary>
		/// Cubic in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CubeIn(Single t)
		{
			return (Single) ( t * t * t);
		}
		
		/// <summary>
		/// Cubic out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CubeOut(Single t)
		{
			return (Single) ( 1 + (--t) * t * t);
		}
		
		/// <summary>
		/// Cubic in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CubeInOut(Single t)
		{
			return (Single) ( t <= .5 ? t * t * t * 4 : 1 + (--t) * t * t * 4);
		}
		
		/// <summary>
		/// Quart in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuartIn(Single t)
		{
			return (Single) ( t * t * t * t);
		}
		
		/// <summary>
		/// Quart out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuartOut(Single t)
		{
			return (Single) ( 1 - (t-=1) * t * t * t);
		}
		
		/// <summary>
		/// Quart in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuartInOut(Single t)
		{
			return (Single) ( t <= .5 ? t * t * t * t * 8 : (1 - (t = t * 2 - 2) * t * t * t) / 2 + .5);
		}
		
		/// <summary>
		/// Quint in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuintIn(Single t)
		{
			return (Single) ( t * t * t * t * t);
		}
		
		/// <summary>
		/// Quint out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuintOut(Single t)
		{
			return (Single) ( (t = t - 1) * t * t * t * t + 1);
		}
		
		/// <summary>
		/// Quint in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single QuintInOut(Single t)
		{
			return (Single) ( ((t *= 2) < 1) ? (t * t * t * t * t) / 2 : ((t -= 2) * t * t * t * t + 2) / 2);
		}
		
		/// <summary>
		/// Sine in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single SineIn(Single t)
		{
			if (t == 1) return 1;
			return (Single) ( -Math.Cos(Pi2 * t) + 1);
		}
		
		/// <summary>
		/// Sine out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single SineOut(Single t)
		{
			return (Single) ( Math.Sin(Pi2 * t));
		}
		
		/// <summary>
		/// Sine in and out
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single SineInOut(Single t)
		{
			return (Single) ( -Math.Cos(Pi * t) / 2 + .5);
		}
		
		/// <summary>
		/// Bounce in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BounceIn(Single t)
		{
			t = 1 - t;
			if (t < B1) return (Single) ( 1 - 7.5625 * t * t);
			if (t < B2) return (Single) ( 1 - (7.5625 * (t - B3) * (t - B3) + .75));
			if (t < B4) return (Single) ( 1 - (7.5625 * (t - B5) * (t - B5) + .9375));
			return (Single) ( 1 - (7.5625 * (t - B6) * (t - B6) + .984375));
		}
		
		/// <summary>
		/// Bounce out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BounceOut(Single t)
		{
			if (t < B1) return (Single) ( 7.5625 * t * t);
			if (t < B2) return (Single) ( 7.5625 * (t - B3) * (t - B3) + .75);
			if (t < B4) return (Single) ( 7.5625 * (t - B5) * (t - B5) + .9375);
			return (Single) ( 7.5625 * (t - B6) * (t - B6) + .984375);
		}
		
		/// <summary>
		/// Bounce in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BounceInOut(Single t)
		{
			if (t < .5)
			{
				t = 1 - t * 2;
				if (t < B1) return (Single) ( (1 - 7.5625 * t * t) / 2);
				if (t < B2) return (Single) ( (1 - (7.5625 * (t - B3) * (t - B3) + .75)) / 2);
				if (t < B4) return (Single) ( (1 - (7.5625 * (t - B5) * (t - B5) + .9375)) / 2);
				return (Single) ( (1 - (7.5625 * (t - B6) * (t - B6) + .984375)) / 2);
			}
			t = t * 2 - 1;
			if (t < B1) return (Single) ( (7.5625 * t * t) / 2 + .5);
			if (t < B2) return (Single) ( (7.5625 * (t - B3) * (t - B3) + .75) / 2 + .5);
			if (t < B4) return (Single) ( (7.5625 * (t - B5) * (t - B5) + .9375) / 2 + .5);
			return (Single) ( (7.5625 * (t - B6) * (t - B6) + .984375) / 2 + .5);
		}
		
		/// <summary>
		/// Circle in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CircIn(Single t)
		{
			return (Single) ( -(Math.Sqrt(1 - t * t) - 1));
		}
		
		/// <summary>
		/// Circle out
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CircOut(Single t)
		{
			return (Single) ( Math.Sqrt(1 - (t - 1) * (t - 1)));
		}
		
		/// <summary>
		/// Circle in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single CircInOut(Single t)
		{
			return (Single) ( t <= .5 ? (Math.Sqrt(1 - t * t * 4) - 1) / -2 : (Math.Sqrt(1 - (t * 2 - 2) * (t * 2 - 2)) + 1) / 2);
		}
		
		/// <summary>
		/// Exponential in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ExpoIn(Single t)
		{
			return (Single) ( Math.Pow(2, 10 * (t - 1)));
		}
		
		/// <summary>
		/// Exponential out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ExpoOut(Single t)
		{
			if (t == 1) return 1;
			return (Single) ( -Math.Pow(2, -10 * t) + 1);
		}
		
		/// <summary>
		/// Exponential in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single ExpoInOut(Single t)
		{
			if (t == 1) return 1;
			return (Single) ( t < .5 ? Math.Pow(2, 10 * (t * 2 - 1)) / 2 : (-Math.Pow(2, -10 * (t * 2 - 1)) + 2) / 2);
		}
		
		/// <summary>
		/// Back in.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BackIn(Single t)
		{
			return (Single) ( t * t * (2.70158 * t - 1.70158));
		}
		
		/// <summary>
		/// Back out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BackOut(Single t)
		{
			return (Single) ( 1 - (--t) * (t) * (-2.70158 * t - 1.70158));
		}
		
		/// <summary>
		/// Back in and out.
		/// </summary>
		/// <param name="t">Time elapsed.</param>
		/// <returns>Eased timescale.</returns>
		public static Single BackInOut(Single t)
		{
			t *= 2;
			if (t < 1) return (Single) ( t * t * (2.70158 * t - 1.70158) / 2);
			t --;
			return (Single) ( (1 - (--t) * (t) * (-2.70158 * t - 1.70158)) / 2 + .5);
		}
	}
}

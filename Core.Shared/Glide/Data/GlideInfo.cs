using System;
using System.Reflection;

namespace Core.Shared.Glide.Data
{
    public class GlideInfo
    {
        private static readonly BindingFlags Flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        public String PropertyName { get; private set; }
        public Type PropertyType { get; private set; }

        private readonly FieldInfo _Field;
        private readonly PropertyInfo _Prop;
        private readonly Object _Target;

        public Object Value
        {
            get => _Field != null ?
                _Field.GetValue(_Target) :
                _Prop.GetValue(_Target, null);

            set
            {
                if (_Field != null) _Field.SetValue(_Target, value);
                else _Prop.SetValue(_Target, value, null);
            }
        }

        public GlideInfo(Object target, PropertyInfo info)
        {
            _Target = target;
            _Prop = info;
            PropertyName = info.Name;
            PropertyType = _Prop.PropertyType;
        }

        public GlideInfo(Object target, FieldInfo info)
        {
            _Target = target;
            _Field = info;
            PropertyName = info.Name;
            PropertyType = info.FieldType;
        }

        public GlideInfo(Object target, String property, Boolean writeRequired = true)
        {
            _Target = target;
            PropertyName = property;

            var targetType = target as Type ?? target.GetType();

            if ((_Field = targetType.GetField(property, Flags)) != null)
            {
                PropertyType = _Field.FieldType;
            }
            else if ((_Prop = targetType.GetProperty(property, Flags)) != null)
            {
                PropertyType = _Prop.PropertyType;
            }
            else
            {
                //	Couldn't find either
                throw new Exception(String.Format("Field or {0} property '{1}' not found on object of type {2}.",
                    writeRequired ? "read/write" : "readable",
                    property, targetType.FullName));
            }
        }
    }
}

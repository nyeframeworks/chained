﻿using System;

namespace Core.Shared.Glide.Lerp
{
    public abstract class Lerper
    {
        [Flags]
        public enum Behavior
        {
            None = 0,
            Reflect = 1
        }

        protected const Single Deg = 180f / (Single)Math.PI;
        protected const Single Rad = (Single)Math.PI / 180f;

        public abstract void Initialize(Object fromValue, Object toValue, Behavior behavior);
        public abstract Object Interpolate(Single t, Object currentValue, Behavior behavior);

    }
}

﻿using System;

namespace Core.Shared.Glide.Lerp
{
    public class NumericLerper : Lerper
    {
        Single _From, _To, _Range;

        public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
        {
            _From = Convert.ToSingle(fromValue);
            _To = Convert.ToSingle(toValue);
            _Range = _To - _From;
        }

        public override Object Interpolate(Single t, Object current, Behavior behavior)
        {
            var value = _From + _Range * t;
            var type = current.GetType();
            return Convert.ChangeType(value, type);
        }
    }
}

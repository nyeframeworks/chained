﻿using System;
using Microsoft.Xna.Framework;

namespace Core.Shared.Glide.Lerp
{
    public class Vector2Lerper : Lerper
    {
        Vector2 _From, _To, _Range;

        public override void Initialize(Object fromValue, Object toValue, Behavior behavior)
        {
            _From = (Vector2)fromValue;
            _To = (Vector2)toValue;
            _Range = _To - _From;
        }

        public override Object Interpolate(Single t, Object current, Behavior behavior)
        {
            var value = _From + _Range * t;
            var type = current.GetType();
            return Convert.ChangeType(value, type);
        }
    }
}

using Core.Shared.Glide.Data;
using Core.Shared.Glide.Lerp;
using Core.Shared.Update;
using System;
using System.Collections.Generic;

namespace Core.Shared.Glide
{
    public class Tween : IUpdatable
    {
        #region Callbacks
        private Func<Single, Single> _Ease;
        private Action _Begin, _Update, _Complete;
        #endregion

        #region Timing
        public Boolean Paused { get; private set; }
        private Single _Delay, _RepeatDelay;
        private readonly Single _Duration;

        private Single _Time;
        #endregion

        private Boolean _FirstUpdate;
        private Int32 _RepeatCount, _TimesRepeated;
        private Lerper.Behavior _Behavior;

        private readonly List<GlideInfo> _Vars;
        private readonly List<Lerper> _Lerpers;
        private readonly List<Object> _Start;
        private readonly List<Object> _End;
        private readonly Dictionary<String, Int32> _VarHash;
        private Tweener _Parent;

        /// <summary>
        /// The time remaining before the tween ends or repeats.
        /// </summary>
        public Single TimeRemaining => _Duration - _Time;

        /// <summary>
        /// A value between 0 and 1, where 0 means the tween has not been started and 1 means that it has completed.
        /// </summary>
        public Single Completion { get { var c = _Time / _Duration; return c < 0 ? 0 : (c > 1 ? 1 : c); } }

        /// <summary>
        /// Whether the tween is currently looping.
        /// </summary>
        public Boolean Looping => _RepeatCount != 0;

        /// <summary>
        /// The object this tween targets. Will be null if the tween represents a timer.
        /// </summary>
        public Object Target { get; private set; }

        public Tween(Object target, Single duration, Single delay, Tweener parent)
        {
            Target = target;
            _Duration = duration;
            _Delay = delay;
            _Parent = parent;

            _FirstUpdate = true;

            _VarHash = new Dictionary<String, Int32>();
            _Vars = new List<GlideInfo>();
            _Lerpers = new List<Lerper>();
            _Start = new List<Object>();
            _End = new List<Object>();
            _Behavior = Lerper.Behavior.None;
        }

        public void AddLerp(Lerper lerper, GlideInfo info, Object from, Object to)
        {
            _VarHash.Add(info.PropertyName, _Vars.Count);
            _Vars.Add(info);

            _Start.Add(from);
            _End.Add(to);

            _Lerpers.Add(lerper);
        }

        public Boolean Active { get; set; } = true;

        public void Update(TimeSpan deltaTime)
        {
            if (_FirstUpdate)
            {
                _FirstUpdate = false;

                var i = _Vars.Count;
                while (i-- > 0)
                {
                    if (_Lerpers[i] != null)
                        _Lerpers[i].Initialize(_Start[i], _End[i], _Behavior);
                }
            }
            else
            {
                if (Paused)
                    return;

                if (_Delay > 0)
                {
                    _Delay -= (Single)deltaTime.TotalSeconds;
                    if (_Delay > 0)
                        return;
                }

                if (Math.Abs(_Time) < Single.Epsilon && _TimesRepeated == 0)
                {
                    _Begin?.Invoke();
                }

                _Time += (Single)deltaTime.TotalSeconds;
                Single setTimeTo = _Time;
                Single t = _Time / _Duration;
                Boolean doComplete = false;

                if (_Time >= _Duration)
                {
                    if (_RepeatCount != 0)
                    {
                        setTimeTo = 0;
                        _Delay = _RepeatDelay;
                        _TimesRepeated++;

                        if (_RepeatCount > 0)
                            --_RepeatCount;

                        if (_RepeatCount < 0)
                            doComplete = true;
                    }
                    else
                    {
                        _Time = _Duration;
                        t = 1;
                        _Parent.Remove(this);
                        doComplete = true;
                    }
                }

                if (_Ease != null)
                    t = _Ease(t);

                Int32 i = _Vars.Count;
                while (i-- > 0)
                {
                    if (_Vars[i] != null)
                        _Vars[i].Value = _Lerpers[i].Interpolate(t, _Vars[i].Value, _Behavior);
                }

                _Time = setTimeTo;

                //	If the timer is zero here, we just restarted.
                //	If reflect mode is on, flip start to end
                if (Math.Abs(_Time) < Single.Epsilon && _Behavior.HasFlag(Lerper.Behavior.Reflect))
                {
                    Reverse();
                }

                _Update?.Invoke();

                if (doComplete)
                {
                    _Complete?.Invoke();
                }
            }
        }

        #region Behavior
        /// <summary>
        /// Apply target values to a starting point before tweening.
        /// </summary>
        /// <param name="values">The values to apply, in an anonymous type ( new { prop1 = 100, prop2 = 0} ).</param>
        /// <returns>A reference to this.</returns>
        public Tween From(Object values)
        {
            var props = values.GetType().GetProperties();
            for (Int32 i = props.Length - 1; i >= 0; --i)
            {
                var property = props[i];
                var propValue = property.GetValue(values, null);

                Int32 index = -1;
                if (_VarHash.TryGetValue(property.Name, out index))
                {
                    //	if we're already tweening this value, adjust the range
                    _Start[index] = propValue;
                }

                //	if we aren't tweening this value, just set it
                var info = new GlideInfo(Target, property.Name, true);
                info.Value = propValue;
            }

            return this;
        }

        /// <summary>
        /// Set the easing function.
        /// </summary>
        /// <param name="ease">The Easer to use.</param>
        /// <returns>A reference to this.</returns>
        public Tween Ease(Func<Single, Single> ease)
        {
            this._Ease = ease;
            return this;
        }

        /// <summary>
        /// Set a function to call when the tween begins (useful when using delays). Can be called multiple times for compound callbacks.
        /// </summary>
        /// <param name="callback">The function that will be called when the tween starts, after the delay.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnBegin(Action callback)
        {
            if (_Begin == null) _Begin = callback;
            else _Begin += callback;
            return this;
        }

        /// <summary>
        /// Set a function to call when the tween finishes. Can be called multiple times for compound callbacks.
        /// If the tween repeats infinitely, this will be called each time; otherwise it will only run when the tween is finished repeating.
        /// </summary>
        /// <param name="callback">The function that will be called on tween completion.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnComplete(Action callback)
        {
            if (_Complete == null) _Complete = callback;
            else _Complete += callback;
            return this;
        }

        /// <summary>
        /// Set a function to call as the tween updates. Can be called multiple times for compound callbacks.
        /// </summary>
        /// <param name="callback">The function to use.</param>
        /// <returns>A reference to this.</returns>
        public Tween OnUpdate(Action callback)
        {
            if (_Update == null) _Update = callback;
            else _Update += callback;
            return this;
        }

        /// <summary>
        /// Enable repeating.
        /// </summary>
        /// <param name="times">Number of times to repeat. Leave blank or pass a negative number to repeat infinitely.</param>
        /// <returns>A reference to this.</returns>
        public Tween Repeat(Int32 times = -1)
        {
            _RepeatCount = times;
            return this;
        }

        /// <summary>
        /// Set a delay for when the tween repeats.
        /// </summary>
        /// <param name="delay">How long to wait before repeating.</param>
        /// <returns>A reference to this.</returns>
        public Tween RepeatDelay(TimeSpan delay)
        {
            _RepeatDelay = (Single)delay.TotalSeconds;
            return this;
        }

        /// <summary>
        /// Sets the tween to reverse every other time it repeats. Repeating must be enabled for this to have any effect.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Reflect()
        {
            _Behavior |= Lerper.Behavior.Reflect;
            return this;
        }

        /// <summary>
        /// Swaps the start and end values of the tween.
        /// </summary>
        /// <returns>A reference to this.</returns>
        public Tween Reverse()
        {
            Int32 i = _Vars.Count;
            while (i-- > 0)
            {
                var s = _Start[i];
                var e = _End[i];

                //	Set start to end and end to start
                _Start[i] = e;
                _End[i] = s;

                _Lerpers[i].Initialize(e, s, _Behavior);
            }

            return this;
        }

        #endregion

        #region Control
        /// <summary>
        /// Cancel tweening given properties.
        /// </summary>
        /// <param name="properties"></param>
        public void Cancel(params String[] properties)
        {
            var canceled = 0;
            for (Int32 i = 0; i < properties.Length; ++i)
            {
                var index = 0;
                if (!_VarHash.TryGetValue(properties[i], out index))
                    continue;

                _VarHash.Remove(properties[i]);
                _Vars[index] = null;
                _Lerpers[index] = null;
                _Start[index] = null;
                _End[index] = null;

                canceled++;
            }

            if (canceled == _Vars.Count)
                Cancel();
        }

        /// <summary>
        /// Deregister tweens from the tweener without calling their complete functions.
        /// </summary>
        public void Cancel()
        {
            _Parent.Remove(this);
        }

        /// <summary>
        /// Assign tweens their final value and remove them from the tweener.
        /// </summary>
        public void CancelAndComplete()
        {
            _Time = _Duration;
            _Update = null;
            _Parent.Remove(this);
        }

        /// <summary>
        /// Set tweens to pause. They won't update and their delays won't tick down.
        /// </summary>
        public void Pause()
        {
            Paused = true;
        }

        /// <summary>
        /// Toggle tweens' paused value.
        /// </summary>
        public void PauseToggle()
        {
            Paused = !Paused;
        }

        /// <summary>
        /// Resumes tweens from a paused state.
        /// </summary>
        public void Resume()
        {
            Paused = false;
        }
        #endregion
    }
}
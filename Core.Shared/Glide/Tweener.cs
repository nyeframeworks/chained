﻿using Core.Shared.Entities;
using Core.Shared.Glide.Data;
using Core.Shared.Update;
using System;
using System.Collections.Generic;

namespace Core.Shared.Glide
{
    [Entity("Tweener")]
    public partial class Tweener : IUpdatable, IDisposable
    {
        private readonly Dictionary<Object, List<Tween>> _Tweens;
        private readonly List<Tween> _ToRemove;
        private readonly List<Tween> _ToAdd;
        private readonly List<Tween> _AllTweens;

        public bool Active { get; set; } = true;

        public Tweener()
        {
            _Tweens = new Dictionary<Object, List<Tween>>();
            _ToRemove = new List<Tween>();
            _ToAdd = new List<Tween>();
            _AllTweens = new List<Tween>();
        }

        /// <summary>
        /// <para>Tweens a set of properties on an object.</para>
        /// <para>To tween instance properties/fields, pass the object.</para>
        /// <para>To tween static properties/fields, pass the type of the object, using typeof(ObjectType) or object.GetType().</para>
        /// </summary>
        /// <param name="target">The object or type to tween.</param>
        /// <param name="values">The values to tween to, in an anonymous type ( new { prop1 = 100, prop2 = 0} ).</param>
        /// <param name="duration">Duration of the tween in seconds.</param>
        /// <param name="delay">Delay before the tween starts, in seconds.</param>
        /// <param name="overwrite">Whether pre-existing tweens should be overwritten if this tween involves the same properties.</param>
        /// <returns>The tween created, for setting properties on.</returns>
        public Tween Tween<T>(T target, Object values, Single duration, Single delay = 0, Boolean overwrite = true) where T : class
        {
            if (target == null) throw new ArgumentNullException(nameof(target));

            //	Prevent tweening on structs if you cheat by casting target as Object
            var targetType = target.GetType();
            if (targetType.IsValueType)
                throw new Exception("Target of tween cannot be a struct!");

            var tween = new Tween(target, duration, delay, this);
            _ToAdd.Add(tween);

            if (values == null) // valid in case of manual timer
                return tween;

            var props = values.GetType().GetProperties();
            for (Int32 i = props.Length - 1; i >= 0; --i)
            {
                if (overwrite && _Tweens.TryGetValue(target, out var library))
                {
                    for (Int32 j = library.Count - 1; j >= 0; j--)
                        library[j].Cancel(props[i].Name);
                }

                var property = props[i];
                var info = new GlideInfo(target, property.Name);
                var to = new GlideInfo(values, property.Name, false);

                tween.AddLerp(CreateLerper(info.PropertyType), info, info.Value, to.Value);
            }

            AddAndRemove();
            return tween;
        }

        /// <summary>
        /// Starts a simple timer for setting up callback scheduling.
        /// </summary>
        /// <param name="duration">How long the timer will run for, in seconds.</param>
        /// <param name="delay">How long to wait before starting the timer, in seconds.</param>
        /// <returns>The tween created, for setting properties.</returns>
        public Tween Timer(Single duration, Single delay = 0)
        {
            var tween = new Tween(null, duration, delay, this);
            AddAndRemove();
            _ToAdd.Add(tween);
            return tween;
        }

        /// <summary>
        /// Deregister tweens from the tweener without calling their complete functions.
        /// </summary>
        public void Cancel()
        {
            _ToRemove.AddRange(_AllTweens);
        }

        /// <summary>
        /// Assign tweens their final value and remove them from the tweener.
        /// </summary>
        public void CancelAndComplete()
        {
            for (Int32 i = _AllTweens.Count - 1; i >= 0; --i)
                _AllTweens[i].CancelAndComplete();
        }

        /// <summary>
        /// Set tweens to pause. They won't update and their delays won't tick down.
        /// </summary>
        public void Pause()
        {
            for (Int32 i = _AllTweens.Count - 1; i >= 0; --i)
            {
                var tween = _AllTweens[i];
                tween.Pause();
            }
        }

        /// <summary>
        /// Toggle tweens' paused value.
        /// </summary>
        public void PauseToggle()
        {
            for (Int32 i = _AllTweens.Count - 1; i >= 0; --i)
            {
                var tween = _AllTweens[i];
                tween.PauseToggle();
            }
        }

        /// <summary>
        /// Resumes tweens from a paused state.
        /// </summary>
        public void Resume()
        {
            for (Int32 i = _AllTweens.Count - 1; i >= 0; --i)
            {
                var tween = _AllTweens[i];
                tween.Resume();
            }
        }

        /// <summary>
        /// Updates the tweener and all objects it contains.
        /// </summary>
        public void Update(TimeSpan deltaTime)
        {
            foreach (var tween in _AllTweens.ToArray())
            {
                tween.Update(deltaTime);
            }

            AddAndRemove();
        }

        public void Remove(Tween tween)
        {
            _ToRemove.Add(tween);
        }

        private void AddAndRemove()
        {
            for (Int32 i = _ToAdd.Count - 1; i >= 0; --i)
            {
                var tween = _ToAdd[i];
                _AllTweens.Add(tween);
                if (tween.Target == null) continue; //	don't sort timers by target

                if (!_Tweens.TryGetValue(tween.Target, out var list)) _Tweens[tween.Target] = list = new List<Tween>();

                list.Add(tween);
            }

            for (Int32 i = _ToRemove.Count - 1; i >= 0; --i)
            {
                var tween = _ToRemove[i];
                _AllTweens.Remove(tween);
                if (tween.Target == null) continue; // see above

                if (_Tweens.TryGetValue(tween.Target, out var list))
                {
                    list.Remove(tween);
                    if (list.Count == 0)
                    {
                        _Tweens.Remove(tween.Target);
                    }
                }

                _AllTweens.Remove(tween);
            }

            _ToAdd.Clear();
            _ToRemove.Clear();
        }

        #region Target control
        /// <summary>
        /// Cancel all tweens with the given target.
        /// </summary>
        /// <param name="target">The object being tweened that you want to cancel.</param>
        public void TargetCancel(Object target)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
            {
                list[i].Cancel();
            }
        }

        /// <summary>
        /// Cancel tweening named properties on the given target.
        /// </summary>
        /// <param name="target">The object being tweened that you want to cancel properties on.</param>
        /// <param name="properties">The properties to cancel.</param>
        public void TargetCancel(Object target, params String[] properties)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
            {
                list[i].Cancel(properties);
            }
        }

        /// <summary>
        /// Cancel, complete, and call complete callbacks for all tweens with the given target..
        /// </summary>
        /// <param name="target">The object being tweened that you want to cancel and complete.</param>
        public void TargetCancelAndComplete(Object target)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
            {
                list[i].CancelAndComplete();
            }
        }


        /// <summary>
        /// Pause all tweens with the given target.
        /// </summary>
        /// <param name="target">The object being tweened that you want to pause.</param>
        public void TargetPause(Object target)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
            {
                list[i].Pause();
            }
        }

        /// <summary>
        /// Toggle the pause state of all tweens with the given target.
        /// </summary>
        /// <param name="target">The object being tweened that you want to toggle pause.</param>
        public void TargetPauseToggle(Object target)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
            {
                list[i].PauseToggle();
            }
        }

        /// <summary>
        /// Resume all tweens with the given target.
        /// </summary>
        /// <param name="target">The object being tweened that you want to resume.</param>
        public void TargetResume(Object target)
        {
            if (!_Tweens.TryGetValue(target, out var list)) return;
            for (Int32 i = list.Count - 1; i >= 0; --i)
                list[i].Resume();
        }
        #endregion

        ~Tweener()
        {

        }

        public void Dispose()
        {
            _AllTweens.Clear();
            _ToAdd.Clear();
            _ToRemove.Clear();
            _Tweens.Clear();
        }
    }
}
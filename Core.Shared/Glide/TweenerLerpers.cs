﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Shared.Glide.Lerp;

namespace Core.Shared.Glide
{
    public partial class Tweener
    {
        private static readonly Dictionary<Type, ConstructorInfo> RegisteredLerpers = new Dictionary<Type, ConstructorInfo>();

        static Tweener()
        {
            var numericTypes = new Type[] {
                typeof(Int16),
                typeof(Int32),
                typeof(Int64),
                typeof(UInt16),
                typeof(UInt32),
                typeof(UInt64),
                typeof(Single),
                typeof(Double),
            };
            for (Int32 i = numericTypes.Length - 1; i >= 0; i--) SetLerper<NumericLerper>(numericTypes[i]);

            SetLerper<Vector2Lerper>(typeof(Microsoft.Xna.Framework.Vector2));
        }

        /// <summary>
        /// Associate a Lerper class with a property type.
        /// </summary>
        /// <typeparam name="TLerper">The Lerper class to use for properties of the given type.</typeparam>
        /// <param name="propertyType">The type of the property to associate the given Lerper with.</param>
        public static void SetLerper<TLerper>(Type propertyType) where TLerper : Lerper, new()
        {
            SetLerper(typeof(TLerper), propertyType);
        }

        /// <summary>
        /// Associate a Lerper type with a property type.
        /// </summary>
        /// <param name="lerperType">The type of the Lerper to use for properties of the given type.</param>
        /// <param name="propertyType">The type of the property to associate the given Lerper with.</param>
        public static void SetLerper(Type lerperType, Type propertyType)
        {
            RegisteredLerpers[propertyType] = lerperType.GetConstructor(Type.EmptyTypes);
        }

        private Lerper CreateLerper(Type propertyType)
        {
            if (!RegisteredLerpers.TryGetValue(propertyType, out var lerper)) throw new Exception($"No Lerper found for type {propertyType.FullName}.");

            return (Lerper)lerper.Invoke(null);
        }
    }
}

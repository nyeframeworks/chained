﻿using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Packages;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using Systems.Shared.Graphics;

namespace Core.Shared.Graphics
{
    [Entity("AnimSprite")]
    [RequiresComponents(new Type[] { typeof(DrawComponent), typeof(OriginComponent) })]
    public class AnimSprite : Entity
    {
        private Texture2D[] _Textures;

        private TimeSpan _ElapsedTime;

        private Action<AnimSprite> _EndBehaviour;

        public Int32 Fps { get; set; }

        private TimeSpan ChangeTime => TimeSpan.FromSeconds(1f / Math.Abs(Fps));

        private Int32 _CurrentFrame = 0;

        private String AnimRoot { get; set; }
        private String AnimFormat { get; set; }

        public Vector2 Position
        {
            get => GetComponent<TransformComponent>().Position;
            set => GetComponent<TransformComponent>().Position = value;
        }

        public Int32 CurrentFrame
        {
            get => _CurrentFrame;
            set
            {
                _CurrentFrame = value;
                _CurrentFrame = Math.Min(_CurrentFrame, _Textures.Length - 1);
                _CurrentFrame = Math.Max(_CurrentFrame, 0);
            }
        }

        public Boolean Playing { get; set; }

        public AnimSprite(Package package) : base(package)
        {
            Fps = package.GetValue<Int32>("FPS");
            AnimRoot = package.GetValue<String>("AnimRoot");
            AnimFormat = package.GetValue<String>("AnimFormat");
        }

        public override void Start()
        {
            base.Start();

            var content = World.GetSystem<IContentSystem>().Content;

            Int32 frames = Package.GetValue<Int32>("Frames");
            List<Texture2D> texture = new List<Texture2D>();
            for (int i = 0; i < frames; i++)
            {
                var path = Path.Combine(AnimRoot, String.Format(AnimFormat, $"{i:D2}"));
                texture.Add(content.Load<Texture2D>(path));
            }

            _Textures = texture.ToArray();
            GetComponent<DrawComponent>().Texture = _Textures[_CurrentFrame];

            Playing = Package.GetValue<Boolean>("Playing");
            if (Playing)
            {
                Play();
            }
        }

        public void SetEndBehaviour(Action<AnimSprite> endBehaviour)
        {
            _EndBehaviour = endBehaviour;
        }

        public void Play()
        {
            Playing = true;

            GetComponent<DrawComponent>().Texture = _Textures[CurrentFrame];

            for (var i = _LocalCallbacks.Count - 1; i >= 0; i--)
            {
                var localCallback = _LocalCallbacks[i];
                if (localCallback.Frame >= 0) continue;
                localCallback.Action?.Invoke(this);
                if (localCallback.SingleUse) _LocalCallbacks.Remove(localCallback);
            }
        }

        public void Stop()
        {
            Playing = false;
            _CurrentFrame = 0;
        }

        public void Pause()
        {
            Playing = false;
        }

        public void Resume()
        {
            Playing = true;
        }

        public void Restart()
        {
            Playing = true;
            _CurrentFrame = 0;
        }

        public override void Update(TimeSpan deltaTime)
        {
            base.Update(deltaTime);

            if (!Playing) return;
            _ElapsedTime += deltaTime;
            if (_ElapsedTime <= ChangeTime) return;
            _ElapsedTime -= ChangeTime;

            CurrentFrame += Fps > 0 ? 1 : -1;

            GetComponent<DrawComponent>().Texture = _Textures[CurrentFrame];

            if (_CurrentFrame == _Textures.Length - 1) EndReached();
            else FrameReached(_CurrentFrame);
        }

        private void FrameReached(Int32 frame)
        {
            GetComponent<DrawComponent>().Texture = _Textures[CurrentFrame];

            for (var i = _LocalCallbacks.Count - 1; i >= 0; i--)
            {
                var localCallback = _LocalCallbacks[i];
                if (localCallback.Frame != frame) continue;
                localCallback.Action?.Invoke(this);
                if (localCallback.SingleUse) _LocalCallbacks.Remove(localCallback);
            }
        }

        private void EndReached()
        {
            _EndBehaviour?.Invoke(this);

            for (var i = _LocalCallbacks.Count - 1; i >= 0; i--)
            {
                var localCallback = _LocalCallbacks[i];
                if (localCallback.Frame != _Textures.Length) continue;
                localCallback.Action?.Invoke(this);
                if (localCallback.SingleUse) _LocalCallbacks.Remove(localCallback);
            }
        }

        #region Callbacks

        private class Callback
        {
            /// <summary>-1 for first time start is called, otherwise match the frame it reaches</summary>
            public Int32 Frame { get; set; }
            public Action<AnimSprite> Action { get; set; }
            public Boolean SingleUse { get; set; }
            public Int32 Count { get; set; }
        }

        private readonly List<Callback> _LocalCallbacks = new List<Callback>();


        public void AddStartCallback(Action<AnimSprite> callback, Boolean singleUse = true)
        {
            _LocalCallbacks.Add(new Callback()
            {
                Frame = -1,
                SingleUse = singleUse,
                Action = callback,
                Count = 0
            });
        }

        public void AddFrameCallback(Int32 frame, Action<AnimSprite> callback, Boolean singleUse = true)
        {
            _LocalCallbacks.Add(new Callback()
            {
                Frame = frame,
                SingleUse = singleUse,
                Action = callback,
                Count = 0
            });
        }

        public void AddEndCallback(Action<AnimSprite> callback, Boolean singleUse = true)
        {
            _LocalCallbacks.Add(new Callback()
            {
                Frame = _Textures.Length,
                SingleUse = singleUse,
                Action = callback,
                Count = 0
            });
        }

        #endregion
    }
}

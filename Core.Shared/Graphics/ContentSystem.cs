﻿using Systems.Shared.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Core.Shared.Graphics
{
    public class ContentSystem : System, IContentSystem
    {
        public ContentManager Content { get; }

        public ContentSystem(ContentManager cm)
        {
            Content = cm;
        }
    }
}

﻿using Core.Shared.Components;
using Definitions.Camera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Systems.Shared.Graphics;
using Systems.Shared.World;

namespace Core.Shared.Graphics
{
    public class DrawSystem : Core.Shared.System, IDrawSystem
    {
        private readonly List<IEntity> _Drawables = new List<IEntity>();

        private readonly ICamera _Camera;

        public DrawSystem(ICamera camera)
        {
            _Camera = camera;
        }

        public void DrawAll(SpriteBatch spritebatch)
        {
            spritebatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                transformMatrix: _Camera.TransformComponent.Absolute);

            foreach (var entity in _Drawables)
            {
                VisibleComponent visComp = entity.GetComponent<VisibleComponent>();
                if (!visComp.Visible) continue;

                DrawComponent drawComp = entity.GetComponent<DrawComponent>();
                TransformComponent transformComp = entity.GetComponent<TransformComponent>();

                switch (drawComp.Type)
                {
                    case DrawComponent.DrawType.Texture2D:

                        var texture = drawComp.Texture;
                        if (texture == null) break;

                        var sourceRect = new Rectangle(0, 0, texture.Width, texture.Height);

                        var pos = transformComp.AbsolutePosition;

                        var col = entity.GetComponent<ColourComponent>()?.Value ?? Color.White;
                        var rot = transformComp.Rotation;

                        var originComp = entity.GetComponent<OriginComponent>();
                        var origin = originComp?.Value ?? Vector2.Zero;
                        if (originComp is OriginPercentComponent o)
                        {
                            origin = new Vector2(texture.Width * o.Value.X, texture.Height * o.Value.Y);
                        }

                        var scale = transformComp.Scale;

                        var z = drawComp.Z;

                        spritebatch.Draw(texture, pos, sourceRect, col, rot, origin, scale, SpriteEffects.None, z);

                        break;
                    case DrawComponent.DrawType.SpriteText:

                        break;
                }
            }

            spritebatch.End();
        }

        public override void MessageReceived(Message message)
        {
            base.MessageReceived(message);

            if (message.Content == "EntityCreated" && message.Data is IEntity newEntity)
            {
                if (newEntity.GetComponent<DrawComponent>() != null)
                {
                    _Drawables.Add(newEntity);
                }
            }
            else if (message.Content == "EntityDestroyed" && message.Data is IEntity oldEntity)
            {
                if (_Drawables.Contains(oldEntity))
                {
                    _Drawables.Remove(oldEntity);
                }
            }
        }
    }
}

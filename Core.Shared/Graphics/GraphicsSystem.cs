﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Systems.Shared.Graphics;

namespace Core.Shared.Graphics
{
    public class GraphicsSystem : System, IGraphicsSystem
    {
        private readonly GraphicsDeviceManager _Gdm;

        public GraphicsDeviceManager Manager => _Gdm;
        public GraphicsDevice Device => _Gdm.GraphicsDevice;

        public bool Active { get; set; } = true;

        public GraphicsSystem(GraphicsDeviceManager gdm)
        {
            _Gdm = gdm;
        }

        public void Update(TimeSpan deltaTime)
        {

        }
    }
}

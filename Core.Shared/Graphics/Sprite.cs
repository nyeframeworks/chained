﻿using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Packages;

namespace Core.Shared.Graphics
{
    [Entity("Sprite")]
    [RequiresComponents(typeof(DrawComponent))]
    public class Sprite : Entity
    {
        public Sprite(Package package) : base(package)
        {

        }
    }
}

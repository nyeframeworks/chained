﻿using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Packages;
using Definitions.Camera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Core.Shared.Graphics
{
    public class SpriteText : Entity
    {
        private SpriteFont _SpriteFont;

        public Vector2 Origin { get; set; }

        public Vector2 Scale { get; set; } = new Vector2(1, 1);

        public Vector2 Size => _SpriteFont.MeasureString(Text);

        public Color Colour { get; set; } = Color.White;

        public Single Z { get; set; }

        public String Text { get; set; }

        public Vector2 Position
        {
            get => GetComponent<TransformComponent>().Position;
            set => GetComponent<TransformComponent>().Position = value;
        }

        public SpriteText(Package package) : base(package)
        {
        }

        public void SetFont(SpriteFont font)
        {
            _SpriteFont = font;
        }

        public void Draw(ICamera camera, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_SpriteFont, Text, camera.GetWorldPosition(Position), Colour, 0f, Origin, Scale, SpriteEffects.None, Z);
        }
    }
}

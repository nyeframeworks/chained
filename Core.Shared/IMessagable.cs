﻿using Systems.Shared.World;

namespace Core.Shared
{
    public interface IMessagable
    {
        void MessageReceived(Message message);
    }
}

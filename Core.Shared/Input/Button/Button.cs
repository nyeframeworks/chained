﻿using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Packages;
using Definitions.Camera;
using Definitions.Input;
using Definitions.Input.Callbacks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Core.Shared.Input.Button
{
    public class Button : Entity, IMousePressedListener, IMouseReleaseListener
    {
        public Boolean Active { get; set; }

        public Action<Button> OnPressed { get; set; }
        public Action<Button> OnReleased { get; set; }

        public ButtonState State { get; set; }

        public Single Z { get; set; }

        private Rectangle _HitBox;
        private readonly Dictionary<ButtonState, Texture2D> _Textures = new Dictionary<ButtonState, Texture2D>();

        public Vector2 Position
        {
            get => GetComponent<TransformComponent>().Position;
            set => GetComponent<TransformComponent>().Position = value;
        }

        public Button(Package package) : base(package)
        {

        }

        protected override void OnWorldSet()
        {
            base.OnWorldSet();

            World.GetSystem<IInputSystem>().Register((IMousePressedListener)this);
            World.GetSystem<IInputSystem>().Register((IMouseReleaseListener)this);
        }

        public void SetHitBox(Rectangle hitbox)
        {
            _HitBox = hitbox;
        }

        public void SetTexture(ButtonState state, Texture2D texture)
        {
            if (!_Textures.ContainsKey(state)) _Textures.Add(state, null);
            _Textures[state] = texture;
        }

        public void Draw(ICamera camera, SpriteBatch spriteBatch)
        {
            Texture2D texture = _Textures[ButtonState.Released];
            if (_Textures.TryGetValue(State, out Texture2D t)) texture = t;

            var sourceRect = new Rectangle(0, 0, texture.Width, texture.Height);
            spriteBatch.Draw(texture, Position, sourceRect, Color.White, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, Z);
        }

        private Boolean _Pressed;

        public void MousePressed(Vector2 mousePosition)
        {
            var p = Position.ToPoint();
            var hitBox = new Rectangle(_HitBox.X + p.X, _HitBox.Y + p.Y, _HitBox.Width, _HitBox.Height);
            if (!hitBox.Contains(mousePosition.ToPoint())) return;
            _Pressed = true;
            State = ButtonState.Pressed;
            OnPressed?.Invoke(this);
        }

        public void MouseReleased(Vector2 mousePosition)
        {
            var p = Position.ToPoint();
            var hitBox = new Rectangle(_HitBox.X + p.X, _HitBox.Y + p.Y, _HitBox.Width, _HitBox.Height);

            var hit = hitBox.Contains(mousePosition.ToPoint());
            if (!_Pressed && !hit) return;

            _Pressed = false;
            State = ButtonState.Released;

            if (!hit) return;
            OnReleased?.Invoke(this);
        }

        public override void Dispose()
        {
            base.Dispose();
            World.GetSystem<IInputSystem>().Deregister((IMousePressedListener)this);
            World.GetSystem<IInputSystem>().Deregister((IMouseReleaseListener)this);
        }
    }
}

﻿using Definitions.Camera;
using Definitions.Input;
using Definitions.Input.Callbacks;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Shared.Input
{
    public class InputSystem : Shared.System, IInputSystem
    {
        private ICamera _Camera;
        private MouseState _PreviousState;

        public InputSystem(ICamera camera)
        {
            _Camera = camera;
        }

        public override void Update(TimeSpan deltaTime)
        {
            base.Update(deltaTime);

            var state = Mouse.GetState();

            //If the left button has been pressed, and previously wasn't, then it has been pressed
            if (state.LeftButton == ButtonState.Pressed && _PreviousState.LeftButton == ButtonState.Released)
            {
                var cb = _InputCallbacks.OfType<IMousePressedListener>().ToList();
                for (var i = cb.Count - 1; i >= 0; i--)
                {
                    IMousePressedListener mousePressedListener = cb[i];
                    var p = _Camera.GetWorldPosition(state.Position.ToVector2());
                    mousePressedListener.MousePressed(p);
                }
            }
            //If the left button has been released, and previously was pressed, the left button has been released
            else if (state.LeftButton == ButtonState.Released && _PreviousState.LeftButton == ButtonState.Pressed)
            {
                var cb = _InputCallbacks.OfType<IMouseReleaseListener>().ToList();
                for (var i = cb.Count - 1; i >= 0; i--)
                {
                    IMouseReleaseListener mousePressed = cb[i];
                    var p = _Camera.GetWorldPosition(state.Position.ToVector2());
                    mousePressed.MouseReleased(p);
                }
            }

            _PreviousState = state;
        }

        #region Callbacks

        private readonly List<IInputCallback> _InputCallbacks = new List<IInputCallback>();

        #region PressedCallbacks

        public void Register(IMousePressedListener item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMousePressedListener item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion

        #region ReleasedCallbacks

        public void Register(IMouseReleaseListener item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMouseReleaseListener item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion

        public void Register(IMouseCallback item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMouseCallback item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion

        public void Dispose()
        {

        }
    }
}

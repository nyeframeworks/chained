﻿using Definitions.Camera;
using Definitions.Input;
using Definitions.Input.Callbacks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Shared.Input
{
    public class MobileInputService : IInputSystem
    {
        private class TouchInput
        {
            public Int32 TouchID { get; set; }

            public Vector2 Position { get; set; }

            public Boolean Pressed { get; set; }

            public Boolean Released { get; set; }

            public Boolean Checked { get; set; }
        }

        private ICamera _Camera;

        private List<TouchInput> _Touches = new List<TouchInput>();

        public Boolean Active { get; set; }

        public MobileInputService(ICamera camera)
        {
            _Camera = camera;
            TouchPanel.EnableMouseTouchPoint = true;
        }

        public void Update(TimeSpan deltaTime)
        {
            TouchCollection touchCollection = TouchPanel.GetState();

            foreach (var touchInput in _Touches) touchInput.Checked = false;

            //Find all input currently being pressed on the the screen
            foreach (TouchLocation location in touchCollection)
            {
                TouchInput t = _Touches.FirstOrDefault(s => s.TouchID == location.Id);
                if (t == null)
                {
                    t = new TouchInput
                    {
                        TouchID = location.Id,
                        Position = location.Position,
                        Released = false,
                        Checked = true,
                        Pressed = true
                    };
                    _Touches.Add(t);
                }

                //If this ID wasn't previously pressed, then it has just been pressed!
                if (t.Pressed && t.Checked)
                {
                    t.Pressed = true;
                    t.Checked = true;
                    t.Released = false;

                    var cb = _InputCallbacks.OfType<IMousePressedListener>().ToList();
                    for (var i = cb.Count - 1; i >= 0; i--)
                    {
                        IMousePressedListener mousePressedListener = cb[i];
                        var p = _Camera.GetWorldPosition(location.Position);
                        mousePressedListener.MousePressed(p);
                    }
                }
            }

            //For all released inputs trigger events and then remove them
            foreach (var touchInput in _Touches)
            {
                if (!touchInput.Checked && touchInput.Pressed)
                {
                    touchInput.Checked = true;
                    touchInput.Pressed = false;
                    touchInput.Released = true;

                    var cb = _InputCallbacks.OfType<IMouseReleaseListener>().ToList();
                    for (var i = cb.Count - 1; i >= 0; i--)
                    {
                        IMouseReleaseListener mousePressedListener = cb[i];
                        var p = _Camera.GetWorldPosition(touchInput.Position);
                        mousePressedListener.MouseReleased(p);
                    }
                }
            }
        }

        public void Dispose()
        {

        }

        #region Callbacks

        private readonly List<IInputCallback> _InputCallbacks = new List<IInputCallback>();

        #region PressedCallbacks

        public void Register(IMousePressedListener item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMousePressedListener item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion

        #region ReleasedCallbacks

        public void Register(IMouseReleaseListener item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMouseReleaseListener item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion

        public void Register(IMouseCallback item)
        {
            if (_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Add(item);
        }

        public void Deregister(IMouseCallback item)
        {
            if (!_InputCallbacks.Contains(item)) return;
            _InputCallbacks.Remove(item);
        }

        #endregion
    }
}

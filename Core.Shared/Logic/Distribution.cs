﻿using Definitions.Logic;
using System;
using System.Collections.Generic;

namespace Core.Shared.Logic
{
    public class Distribution : Core.Shared.System, IDistribution
    {
        public T Get<T>(Dictionary<T, Int32> weightedValues, IRandom random)
        {
            List<T> items = new List<T>();
            foreach (var weightedValue in weightedValues)
            {
                for (int i = 0; i < weightedValue.Value; i++)
                {
                    items.Add(weightedValue.Key);
                }
            }

            var index = random.Next(items.Count);
            return items[index];
        }

        public void Dispose()
        {

        }
    }
}

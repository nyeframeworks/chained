﻿using Core.Shared;
using System;
using Core.Shared.Entities;
using Core.Shared.Packages;

namespace Core.Shared.Logic
{
    public class GameState : Entity
    {
        public String Name { get; set; }

        public GameState(String name = null) : base(new Package("GameState"))
        {
            if (name == null) name = $"{Guid.NewGuid()}-{GetType()}";
            Name = name;
        }

        public virtual void Enter()
        {

        }

        public virtual void Update(TimeSpan deltaTime)
        {

        }

        public virtual Boolean IsComplete()
        {
            return NextState != null;
        }

        #region Next_State

        public GameState NextState { get; set; }

        #endregion

        public virtual void Exit()
        {

        }

        public void Dispose()
        {

        }
    }
}

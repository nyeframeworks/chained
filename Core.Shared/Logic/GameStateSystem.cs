﻿using Core.Shared.Entities;
using Core.Shared.Packages;
using System;

namespace Core.Shared.Logic
{
    public class GameStateSystem : Entity
    {
        private GameState _ActiveState;

        public GameStateSystem(Package package) : base(package)
        {
        }

        public void Start(GameState startState)
        {
            if (_ActiveState != null) throw new Exception($"This system has already been started");
            _ActiveState = startState;
            //TODO Register to update service

            _ActiveState.Enter();
        }

        public override void Update(TimeSpan deltaTime)
        {
            base.Update(deltaTime);
            _ActiveState.Update(deltaTime);
            if (!_ActiveState.IsComplete()) return;
            _ActiveState.Exit();
            _ActiveState = _ActiveState.NextState;
            _ActiveState.Enter();
        }

        public void Stop()
        {
            //TODO Deregister to update service
            _ActiveState.Exit();
            _ActiveState = null;
        }
    }
}

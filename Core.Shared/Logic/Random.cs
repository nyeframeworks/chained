﻿using Definitions.Logic;
using System;

namespace Core.Shared.Logic
{
    public class Random : Core.Shared.System, IRandom
    {
        private readonly global::System.Random _Random;

        public Random(Int32 seed)
        {
            _Random = new global::System.Random(seed);
        }

        public int Next(int max)
        {
            return _Random.Next(max);
        }

        public void Dispose()
        {

        }
    }
}

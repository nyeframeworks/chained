﻿using System;
using System.Collections.Generic;

namespace Core.Shared.Packages
{
    public sealed class Package
    {
        public String Type { get; }

        public List<Tuple<String, Object>> Data { get; } = new List<Tuple<string, object>>();

        public Package(String type)
        {
            Type = type;
        }

        public void Add(Tuple<String, Object> data)
        {
            Data.Add(data);
        }

        public void AddRange(List<Tuple<String, Object>> data)
        {
            Data.AddRange(data);
        }

        public static implicit operator List<Tuple<String, Object>>(Package p)
        {
            return p.Data;
        }
    }
}

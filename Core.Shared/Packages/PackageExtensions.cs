﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Shared.Packages
{
    public static class PackageExtensions
    {
        public static T GetValue<T>(this List<Tuple<String, Object>> data, String name)
        {
            if (typeof(T) == typeof(Point))
            {
                var p = new Point(data.GetValue<Int32>($"{name}-X"), data.GetValue<Int32>($"{name}-Y"));
                return (T)Convert.ChangeType(p, typeof(T));
            }

            if (typeof(T) == typeof(Vector2))
            {
                var p = new Vector2(data.GetValue<Single>($"{name}-X"), data.GetValue<Single>($"{name}-Y"));
                return (T)Convert.ChangeType(p, typeof(T));
            }

            if (typeof(T) == typeof(Color))
            {
                var r = data.GetValue<Single>($"{name}-R");
                var g = data.GetValue<Single>($"{name}-G");
                var b = data.GetValue<Single>($"{name}-B");
                var a = data.GetValue<Single>($"{name}-A");

                var all = new Single[] { r, g, b, a };
                var p = new Color(r, g, b, a);
                //If any colour values exceed 1, then treat them as int's instead.
                if (all.Any(s => s > 1))
                {
                    p = new Color((Int32)r, (Int32)g, (Int32)b, (Int32)a);
                }
                return (T)Convert.ChangeType(p, typeof(T));
            }

            return data.Value<T>(name);
        }

        public static T GetValue<T>(this Package package, String name)
        {
            return package.Data.GetValue<T>(name);
        }

        public static T Value<T>(this List<Tuple<String, Object>> data, String name)
        {
            var valueData = data.Where(s => !(s.Item2 is Package)).ToList();
            var value = valueData.FirstOrDefault(s => s.Item1 == name)?.Item2 as String;
            if (value == null) return default(T);

            var e = Extract(value, typeof(T));
            if (e == null) return default(T);
            return (T)e;
        }

        public static T Value<T>(this Package package, String name)
        {
            return package.Data.Value<T>(name);
        }

        public static T[] GetArray<T>(this List<Tuple<String, Object>> data, String name)
        {
            String values = data.GetValue<String>(name);
            var array = values.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToArray();

            var at = Array.ConvertAll(array, s => Extract(s, typeof(T)));

            T[] endArray = new T[at.Length];
            for (var i = 0; i < at.Length; i++)
            {
                endArray[i] = (T)Convert.ChangeType(at[i], typeof(T));
            }

            return endArray;
        }

        public static T[] GetArray<T>(this Package package, String name)
        {
            return GetArray<T>(package.Data, name);
        }

        private static Object Extract(String value, Type type)
        {
            if (type == typeof(Single))
            {
                Single.TryParse(value, out Single v);
                return Convert.ChangeType(v, type);
            }
            if (type == typeof(Int32))
            {
                Int32.TryParse(value, out Int32 v);
                return Convert.ChangeType(v, type);
            }

            if (type == typeof(String))
            {
                return Convert.ChangeType(value, type);
            }

            if (type == typeof(Boolean))
            {
                Boolean.TryParse(value, out Boolean b);
                return Convert.ChangeType(b, type);
            }

            return null;
        }
    }
}

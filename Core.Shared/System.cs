﻿using System;
using Systems.Shared;
using Systems.Shared.World;

namespace Core.Shared
{
    public class System : ISystem
    {
        public bool Active { get; set; } = true;

        protected IWorld World { get; private set; }

        public virtual void SetWorld(IWorld world)
        {
            World = world;
        }

        public virtual void Update(TimeSpan deltaTime)
        {

        }

        public virtual void MessageReceived(Message message)
        {

        }

        public virtual void Dispose()
        {

        }
    }
}

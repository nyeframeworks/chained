﻿using System;

namespace Core.Shared.Update
{
    public interface IUpdatable
    {
        Boolean Active { get; set; }

        void Update(TimeSpan deltaTime);
    }
}

﻿using System;
using System.Collections.Generic;
using Systems.Shared.Update;
using Systems.Shared.World;

namespace Core.Shared.Update
{
    public sealed class UpdateSystem : Core.Shared.System, IUpdateSystem
    {
        private readonly List<IUpdatable> _Updatables = new List<IUpdatable>();

        public override void Update(TimeSpan deltaTime)
        {
            base.Update(deltaTime);
            for (var i = _Updatables.Count - 1; i >= 0; i--)
            {
                var u = _Updatables[i];
                if (!u.Active) continue;
                u.Update(deltaTime);
            }
        }

        public override void MessageReceived(Message message)
        {
            base.MessageReceived(message);

            if (message.Content == "EntityCreated" && message.Data is IEntity newEntity)
            {
                if (!_Updatables.Contains(newEntity))
                {
                    _Updatables.Add(newEntity);
                }
            }
            else if (message.Content == "EntityDestroyed" && message.Data is IEntity oldEntity)
            {
                if (_Updatables.Contains(oldEntity))
                {
                    _Updatables.Remove(oldEntity);
                }
            }
        }
    }
}

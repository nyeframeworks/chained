﻿using Core.Shared.Entities;
using System.Xml.Linq;
using Systems.Shared.Data;
using Systems.Shared.World;

namespace Core.Shared.World.Data
{
    public class XMLEntityCreator : IEntityCreator
    {
        public T Create<T>(string data) where T : IEntity
        {
            var package = XElement.Parse(Shared.Data.ReadAll(data)).ToPackage();
            var entity = (T)EntityStore.Creator(package.Type, package);
            return entity;
        }
    }
}

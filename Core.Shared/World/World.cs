﻿using Core.Shared.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using Systems.Shared;
using Systems.Shared.Data;
using Systems.Shared.Graphics;
using Systems.Shared.World;

namespace Core.Shared.World
{
    public class World : IWorld
    {
        private readonly List<ISystem> _AllWorldSystems = new List<ISystem>();

        private readonly List<IEntity> _AllWorldEntities = new List<IEntity>();

        private IEntityCreator _Creator;

        public bool Active { get; set; } = true;

        public void AddSystem<T>(T system) where T : ISystem
        {
            foreach (var allWorldSystem in _AllWorldSystems.ToList())
            {
                if (!(allWorldSystem is T t)) continue;
                t.Dispose();
                _AllWorldSystems.Remove(t);
                break;
            }

            system.SetWorld(this);
            _AllWorldSystems.Add(system);
        }

        public T GetSystem<T>() where T : ISystem
        {
            return (T)_AllWorldSystems.First(s => s is T);
        }

        public void SetCreator(IEntityCreator creator)
        {
            _Creator = creator;
        }

        public T Create<T>(String filePath) where T : IEntity
        {
            var entity = _Creator.Create<T>(filePath);
            entity.SetWorld(this);
            entity.Start();
            _AllWorldEntities.Add(entity);

            BroadcastMessage(new Message("EntityCreated", entity), typeof(ISystem));

            OnEntityCreated<T>(entity);
            return entity;
        }

        protected virtual void OnEntityCreated<T>(T entity) where T : IEntity
        {

        }

        public void Destroy(IEntity entity)
        {
            _AllWorldEntities.Remove(entity);
            BroadcastMessage(new Message("EntityDestroyed", entity), typeof(ISystem));
        }

        public void Update(TimeSpan deltaTime)
        {
            for (var i = _AllWorldSystems.Count - 1; i >= 0; i--)
            {
                var allWorldSystem = _AllWorldSystems[i];
                allWorldSystem.Update(deltaTime);
            }
        }

        public void DrawAll()
        {
            GetSystem<IGraphicsSystem>().Device.Clear(Color.CornflowerBlue);
            GetSystem<IDrawSystem>().DrawAll(new SpriteBatch(GetSystem<GraphicsSystem>().Device));
        }

        public T Get<T>(string name) where T : IEntity
        {
            return _AllWorldEntities.OfType<T>().First(s => name == null || s.Name == name);
        }

        public void BroadcastMessage(Message message, params Type[] filter)
        {
            var all = new List<IMessagable>();
            all.AddRange(!filter.Any() ? _AllWorldSystems : _AllWorldSystems.Where(system => filter.Any(a => a.IsInstanceOfType(system))));

            var selectedEntities = (!filter.Any() ? _AllWorldEntities : _AllWorldEntities.Where(entity =>
            {
                //Does this entity contain instances of ALL the component types passed in the filter?
                Boolean componentMatch = entity.GetComponents().Select(component => component.GetType())
                     .All(ct => filter.Any(ct.IsAssignableFrom));

                //How about does this entity itself, match the type filter itself?
                Boolean entityMatch = filter.Any(e => e.IsInstanceOfType(entity));

                //If either matches are true, send the message to this entity.
                return componentMatch || entityMatch;

            })).ToList();

            all.AddRange(selectedEntities);

            foreach (var msg in all)
            {
                msg.MessageReceived(message);
            }
        }

        public void MessageReceived(Message message, int clientID)
        {

        }

        public void Dispose()
        {

        }

        ~World()
        {

        }
    }
}

﻿using Core.Shared.Packages;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Core.Shared
{
    public static class XElementExtensions
    {
        public static Package ToPackage(this XElement element)
        {
            Package p = new Package(element.Name.LocalName);

            // cater for attributes as properties
            if (element.HasAttributes)
            {
                p.AddRange(element.Attributes().Select(attribute => new Tuple<String, Object>(attribute.Name.LocalName, attribute.Value)).ToList());
            }

            // cater for child nodes as properties, or child objects
            if (element.HasElements)
            {
                foreach (XElement subElement in element.Elements())
                {
                    // if sub element has child elements
                    if (subElement.HasElements)
                    {
                        Package subPackage = new Package(subElement.Name.LocalName);
                        // using a bit of recursion lets us cater for an unknown chain of child elements
                        subPackage.AddRange(subElement.ToPackage());
                        p.Add(new Tuple<string, object>(subPackage.Type, subPackage));
                    }
                    else if (subElement.HasAttributes)
                    {
                        p.AddRange(subElement.Attributes().Select(xAttribute => new Tuple<String, Object>($"{subElement.Name.LocalName}-{xAttribute.Name}", xAttribute.Value)).ToList());
                    }
                    else
                    {
                        p.Add(new Tuple<String, Object>(subElement.Name.LocalName, subElement.Value));
                    }
                }
            }

            return p;
        }
    }

}


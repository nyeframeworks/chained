﻿using Core.Shared;
using Core.Shared.Camera;
using Core.Shared.Components;
using Core.Shared.Entities;
using Core.Shared.Graphics;
using Core.Shared.Input;
using Core.Shared.Logic;
using Core.Shared.Update;
using Core.Shared.World;
using Core.Shared.World.Data;
using Definitions.Camera;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using Systems.Shared.World;
using Random = Core.Shared.Logic.Random;

namespace Example.Project
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class ExampleGame : Game
    {
        private readonly GraphicsDeviceManager _Graphics;
        private IWorld _World;

        public ExampleGame()
        {
            _Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 480,//15 x 32 pixels
                PreferredBackBufferHeight = 640,//20 x 32 pixels
                SupportedOrientations = DisplayOrientation.Portrait | DisplayOrientation.PortraitDown
            };
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Data.Init(Content);

            var camera = new Camera(_Graphics.GraphicsDevice);

            _World = new World();
            _World.AddSystem(new GraphicsSystem(_Graphics));
            _World.AddSystem(new ContentSystem(Content));
            _World.AddSystem(camera);
            _World.AddSystem(new InputSystem(camera));
            _World.AddSystem(new DrawSystem(camera));
            _World.AddSystem(new UpdateSystem());
            _World.AddSystem(new Random(Environment.TickCount));
            _World.AddSystem(new Distribution());

            _World.SetCreator(new XMLEntityCreator());

            base.Initialize();
        }

        private void ModifyCameraZoom(ICamera camera, Int32 width, Int32 height)
        {
            var targetSize = new Vector2(480, 640);
            var actualSize = new Vector2(width, height);

            var ratio = actualSize / targetSize;

            camera.TransformComponent.Scale = new Vector2(ratio.X, ratio.Y);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ModifyCameraZoom(_World.GetSystem<ICamera>(), _Graphics.GraphicsDevice.Viewport.Width, _Graphics.GraphicsDevice.Viewport.Height);

            var assemblies = new List<Assembly>
            {
                Assembly.GetAssembly(typeof(ExampleGame))
            };
            EntityStore.RegisterAll(assemblies);
            ComponentStore.RegisterAll(assemblies);

            _World.Create<Sprite>("Data/ExampleTile.xml");

            var exampleTile = _World.Get<Sprite>("ExampleTile");

            var camera = _World.GetSystem<ICamera>();
            exampleTile.GetComponent<TransformComponent>().Position = camera.TransformComponent.AbsolutePosition + camera.Bounds.Size.ToVector2() / 2f;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _World.Update(gameTime.ElapsedGameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            _World.DrawAll();
            base.Draw(gameTime);
        }
    }
}

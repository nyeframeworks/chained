﻿using Core.Shared.Components;
using Microsoft.Xna.Framework;
using Systems.Shared;

namespace Definitions.Camera
{
    /// <summary>
    /// Represents a player camera.
    /// </summary>
    public interface ICamera : ISystem
    {
        TransformComponent TransformComponent { get; }

        Vector2 GetWorldPosition(Vector2 mousePosition);

        Rectangle Bounds { get; }
    }
}


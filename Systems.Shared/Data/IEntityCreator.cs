﻿using System;
using Systems.Shared.World;

namespace Systems.Shared.Data
{
    public interface IEntityCreator
    {
        T Create<T>(String data) where T : IEntity;
    }
}

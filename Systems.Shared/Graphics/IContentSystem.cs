﻿using Microsoft.Xna.Framework.Content;

namespace Systems.Shared.Graphics
{
    public interface IContentSystem : ISystem
    {
        ContentManager Content { get; }
    }
}

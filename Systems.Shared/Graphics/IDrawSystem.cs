﻿using Microsoft.Xna.Framework.Graphics;

namespace Systems.Shared.Graphics
{
    public interface IDrawSystem : ISystem
    {
        void DrawAll(SpriteBatch spritebatch);
    }
}

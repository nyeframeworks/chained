﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Systems.Shared.Graphics
{
    public interface IGraphicsSystem : ISystem
    {
        GraphicsDeviceManager Manager { get; }

        GraphicsDevice Device { get; }

    }
}

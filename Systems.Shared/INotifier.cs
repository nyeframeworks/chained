﻿namespace Systems.Shared
{
    public interface INotifier<T>
    {
        void Register(T item);

        void Deregister(T item);
    }
}

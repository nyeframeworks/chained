﻿using Core.Shared;
using Core.Shared.Update;
using System;
using Systems.Shared.World;

namespace Systems.Shared
{
    public interface ISystem : IUpdatable, IDisposable, IMessagable
    {
        void SetWorld(IWorld world);
    }
}

﻿namespace Definitions.Input.Callbacks
{
    public interface IMouseCallback : IMousePressedListener, IMouseReleaseListener
    {
    }
}

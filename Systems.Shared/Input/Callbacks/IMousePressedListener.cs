﻿using Microsoft.Xna.Framework;

namespace Definitions.Input.Callbacks
{
    public interface IMousePressedListener : IInputCallback
    {
        void MousePressed(Vector2 mousePosition);
    }
}

﻿using Microsoft.Xna.Framework;

namespace Definitions.Input.Callbacks
{
    public interface IMouseReleaseListener : IInputCallback
    {
        void MouseReleased(Vector2 mousePosition);
    }
}

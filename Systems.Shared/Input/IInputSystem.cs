﻿using Definitions.Input.Callbacks;
using Systems.Shared;

namespace Definitions.Input
{
    public interface IInputSystem : ISystem,
        INotifier<IMousePressedListener>,
        INotifier<IMouseReleaseListener>,
        INotifier<IMouseCallback>
    {

    }
}

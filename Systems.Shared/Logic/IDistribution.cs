﻿using System;
using System.Collections.Generic;
using Systems.Shared;

namespace Definitions.Logic
{
    public interface IDistribution : ISystem
    {
        T Get<T>(Dictionary<T, Int32> weightedValues, IRandom random);
    }
}

﻿using Systems.Shared;
using System;

namespace Definitions.Logic
{
    public interface IRandom : ISystem
    {
        Int32 Next(Int32 max);
    }
}

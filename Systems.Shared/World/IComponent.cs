﻿using System;

namespace Systems.Shared.World
{
    public interface IComponent : IDisposable
    {
        void SetOwner(IEntity entity);

        void SetWorld(IWorld world);

        void Start();
    }
}

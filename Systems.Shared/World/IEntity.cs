﻿using Core.Shared;
using Core.Shared.Update;
using System;

namespace Systems.Shared.World
{
    public interface IEntity : IDisposable, IUpdatable, IMessagable
    {
        Int64 ID { get; }

        String Name { get; }

        void SetWorld(IWorld world);

        void Start();

        void Attach<T>(T entity) where T : IEntity;
        void Detatch<T>(T entity) where T : IEntity;

        void SetParentVisible(Boolean parentVisible);
        void SetParentActive(Boolean parentActive);

        T GetComponent<T>(Func<T, Boolean> predicate = null) where T : IComponent;
        IComponent[] GetComponents(Func<IComponent, Boolean> predicate = null);

        T GetChild<T>(Func<T, Boolean> predicate = null) where T : IEntity;
        IEntity[] GetChildren(Func<IEntity, Boolean> predicate = null);
    }
}

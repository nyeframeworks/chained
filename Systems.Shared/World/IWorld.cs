﻿using System;
using Systems.Shared.Data;

namespace Systems.Shared.World
{
    public interface IWorld : IDisposable
    {
        void AddSystem<T>(T system) where T : ISystem;
        T GetSystem<T>() where T : ISystem;

        void SetCreator(IEntityCreator creator);

        T Create<T>(String filePath) where T : IEntity;
        T Get<T>(String name) where T : IEntity;

        void DrawAll();

        /// <summary>Broadcast to all <see cref="IEntity"/>'s which have all the target <see cref="IComponent"/>'s <see cref="Type"/>'s</summary>
        /// <param name="message"></param>
        /// <param name="filter"></param>
        void BroadcastMessage(Message message, params Type[] filter);

        void MessageReceived(Message message, Int32 clientID);

        void Destroy(IEntity entity);

        void Update(TimeSpan deltaTime);
    }
}

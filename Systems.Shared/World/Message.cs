﻿using System;

namespace Systems.Shared.World
{
    public struct Message
    {
        public Object Data { get; }
        public String Content { get; }

        public Message(string content, object data)
        {
            Content = content;
            Data = data;
        }
    }
}
